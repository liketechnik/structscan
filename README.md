<!--
SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
-->

Traits and derive macros to interactively create structs from terminal input.

# Usage

See https://gitlab.com/liketechnik/scheduling-simulations for an example of using this library.

# License

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
Note that certain configuration and data files are licensed under CC0-1.0.
