// SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use structscan::UserSubmittable;

#[derive(UserSubmittable, PartialEq, Debug, derive_new::new)]
struct Test {
    field: String,
}

#[test]
fn test_user_submittable() {
    let test = Test::from_user(&mut "Works".as_bytes());
    assert_eq!(
        Some(Test {
            field: "Works".to_string()
        }),
        test
    );
}

#[derive(UserSubmittable, PartialEq, Debug, derive_new::new)]
struct TestMultipleFields {
    str_input: String,
    u32_input: u32,
    f32_input: f32,
}

#[test]
fn test_user_submittable_multiple_fields() {
    let test = TestMultipleFields::from_user(&mut "Works\n10\n1.0".as_bytes());
    assert_eq!(
        Some(TestMultipleFields::new("Works".to_string(), 10, 1.0)),
        test
    );
}

#[test]
fn test_user_submittable_multiple_fields_failure() {
    let test = TestMultipleFields::from_user(&mut "Works\n10\nDoes not work".as_bytes());
    assert_eq!(None, test);
}
