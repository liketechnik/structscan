// SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{io::BufRead, str::FromStr};

#[cfg(feature = "derive")]
#[doc(hidden)]
pub use structscan_derive::*;

pub trait InputPart<T>
where
    T: FromStr,
{
    fn name() -> &'static str;

    fn from_user_input(input_reader: &mut impl BufRead) -> Option<T> {
        println!("{}:", Self::name());
        let mut user_input = String::new();
        input_reader
            .read_line(&mut user_input)
            .expect("Failed to read from input");
        match T::from_str(user_input.trim()) {
            Ok(v) => Some(v),
            Err(_) => {
                eprintln!("Invalid input!");
                None
            }
        }
    }
}

impl<T, I> FromUser<T> for I
where
    T: FromStr,
    I: InputPart<T>,
{
    fn from_user(input_reader: &mut impl BufRead) -> Option<T> {
        I::from_user_input(input_reader)
    }
}

impl<T, U, V, W> FromUser<(V, W)> for (T, U)
where
    T: FromUser<V>,
    U: FromUser<W>,
{
    fn from_user(input_reader: &mut impl BufRead) -> Option<(V, W)> {
        let v = match T::from_user(input_reader) {
            Some(v) => v,
            None => return None,
        };
        let w = match U::from_user(input_reader) {
            Some(w) => w,
            None => return None,
        };
        Some((v, w))
    }
}

pub trait FromUser<T> {
    fn from_user(input_reader: &mut impl BufRead) -> Option<T>;
}

pub trait UserSubmittable<T, I>
where
    Self: Sized,
    T: FromUser<I>,
{
    fn name() -> &'static str;

    fn from_input_parts(i: I) -> Self;

    fn from_user(input_reader: &mut impl BufRead) -> Option<Self> {
        println!("Adding new {}", Self::name());
        T::from_user(input_reader).map(Self::from_input_parts)
        // match T::from_user(input_reader) {
        //     Some(v) => Some(Self::from_input_parts(v)),
        //     None => None,
        // }
    }
}
