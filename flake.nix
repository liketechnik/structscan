# SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0

{
  description = "structscan development env flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.05";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { self, nixpkgs, flake-utils, nixpkgs-unstable, rust-overlay, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # create a new overlay
        overlay-unstable = self: super: {
          # expose the packages from nixpkgs-unstable under the unstable attribute
          unstable = inputs.nixpkgs-unstable.legacyPackages.${super.system};
        };
        pkgs = import nixpkgs {
          overlays = [ overlay-unstable (import rust-overlay) ];

          inherit system;
        };
        rust = pkgs.rust-bin.nightly.latest.default.override {
          extensions = [ "rust-src" "cargo" "rustc" "rustfmt" "rust-analysis" "clippy" ];
        };
        rust-analyzer = pkgs.rust-analyzer.override {
          rustSrc = rust;
        };
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = [ 
            rust
            rust-analyzer
            pkgs.unstable.cargo-expand

            pkgs.unstable.reuse
          ];

          # See https://discourse.nixos.org/t/rust-src-not-found-and-other-misadventures-of-developing-rust-on-nixos/11570/3?u=samuela.
          # doesn't seem to be needed with the new overlay?
          # RUST_SRC_PATH = "${pkgs.rust-bin.stable.latest.default.rustLibSrc}";
        };
      }
    );
}
