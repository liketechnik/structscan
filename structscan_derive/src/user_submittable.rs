// SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use quote::{format_ident, quote};

use syn::{spanned::Spanned, Data, DeriveInput, Fields};

pub fn expand(input: DeriveInput) -> syn::Result<proc_macro2::TokenStream> {
    let name = &input.ident;

    let fields = match &input.data {
        Data::Struct(data) => match &data.fields {
            Fields::Named(fields) => &fields.named,
            Fields::Unit => {
                return Err(syn::Error::new(
                    input.span(),
                    r#"`#[derive(UserSubmittable)]` only supports named fields"#,
                ))
            }
            Fields::Unnamed(fields) => {
                return Err(syn::Error::new(
                    fields.span(),
                    r#"`#[derive(UserSubmittable)]` only supports named fields"#,
                ))
            }
        },
        Data::Enum(data) => {
            return Err(syn::Error::new(
                data.enum_token.span(),
                r#"`#[derive(UserSubmittable)]` only supports structs"#,
            ))
        }
        Data::Union(data) => {
            return Err(syn::Error::new(
                data.union_token.span(),
                r#"`#[derive(UserSubmittable)]` only supports structs"#,
            ))
        }
    };

    let input_parts_module = format_ident!("{}_input_parts", name);
    let mut input_part_impls = Vec::new();
    let mut args_ty = None;
    let mut impl_generics_in = None;
    let mut from_input_parts_args_name = None;
    let mut new_params = Vec::new();

    for field in fields.iter().rev() {
        let field_name = &field.ident;
        let field_ty = &field.ty;

        input_part_impls.push(quote! {
            pub struct #field_name;

            impl ::structscan::InputPart<#field_ty> for #field_name {
                fn name() -> &'static str {
                    stringify!(#field_name)
                }
            }
        });

        match &impl_generics_in {
            Some(previous_impl_generics_in) => {
                impl_generics_in =
                    Some(quote!((#input_parts_module::#field_name, #previous_impl_generics_in)));
            }
            None => {
                impl_generics_in = Some(quote!(#input_parts_module::#field_name));
            }
        }

        match &args_ty {
            Some(previous_args_ty) => {
                args_ty = Some(quote!((#field_ty, #previous_args_ty)));
            }
            None => {
                args_ty = Some(quote!(#field_ty));
            }
        }

        match &from_input_parts_args_name {
            Some(prev_from_input_parts_args_name) => {
                from_input_parts_args_name =
                    Some(quote!((#field_name, #prev_from_input_parts_args_name)));
            }
            None => {
                from_input_parts_args_name = Some(quote!(#field_name));
            }
        }

        new_params.push(quote!(#field_name));
    }

    let args_ty = args_ty.unwrap();
    let impl_generics_in = impl_generics_in.unwrap();
    let from_input_parts_args_name = from_input_parts_args_name.unwrap();
    let new_params = new_params.into_iter().rev();

    Ok(quote! {
        // let's just not deal with name changing here and just disable the lints
        #[allow(non_camel_case_types)]
        #[allow(non_snake_case)]
        mod #input_parts_module {
            #(#input_part_impls)*
        }
        /*
        struct StringInput;

        impl ::structscan::InputPart<String> for StringInput {
            fn name() -> &'static str {
                "field"
            }
        }
        */

        impl ::structscan::UserSubmittable<#impl_generics_in, #args_ty/* todo StringInput, String*/> for #name {
            fn name() -> &'static str {
                stringify!(#name)
            }

            fn from_input_parts(#from_input_parts_args_name: #args_ty/* todo string: String */) -> #name {
                #name::new(#(#new_params,)*)
            }
        }
    })
}
