// SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};

mod user_submittable;

#[proc_macro_derive(UserSubmittable)]
pub fn derive_user_submittable(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    user_submittable::expand(input)
        .unwrap_or_else(syn::Error::into_compile_error)
        .into()
}
